 Head | Information |
| ------ | ----------- |
| Logbook entry number | 04 |
| Name  | Kee Soon Win |
| Matrix no.  | 195901 |
| Year  | 4 |
| Subsystem  | Simulation |
| Group  | 7 |


## Tables

| Target | Information |
| ------ | ----------- |
| Agenda  | -Do performance and analysis result base on the model.<br> -CFD analysis on the CATIA designed HAU airframe.|
| Goals  | -To have a good result from the software to analys.|
| Problems  | -Have no experience to perform and using the ansys software.<br> -Few parameters couldn't get from other subsystems|
| Decision  | -We decided to use Ansys to do CFD.|
| Method  | -Use the HAU Airframe design that was previously created in CATIA V5.<br> -Catia drawing transfer to Ansys to get the simulation result|
| Justification  | -Will have the idea on how the simulation process done.<br> -To identify and correct any discrepancies or errors in ANSYS results.|
| Impact  | -Have a good performamce result.|
| Next Step | -Perform the analysis on the CFD again to perform the adjustment at the same time.|







