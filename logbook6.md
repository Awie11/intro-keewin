 Head | Information |
| ------ | ----------- |
| Logbook entry number | 06 |
| Name  | Kee Soon Win |
| Matrix no.  | 195901 |
| Year  | 4 |
| Subsystem  | Simulation |
| Group  | 7 |



## Tables

| Target | Information |
| ------ | ----------- |
| Agenda  | -To get the CL, CD, Cd0 and Cm by using Ansys software.<br> -To plot the graph which is volume vs weight and volume vs lift force.|
| Decision | -We figure out how to use Ansys software together.<br> -we can come out with the best result of the ansys and all the members also understand how is the ansys works.|
| Method | -Having a discussion in discord to make sure everyone in the group get the info up to date.<br> -For those who didn't go to the lab can still learn how to do ansys with other group members|
| Justification | -Refer to the Airship and Design book we just verify our job and results by asking seniors to double confirm it |
| Impact| -Before doing calculation and things we discuss among group members to ensure that we did the right thing. |
| Next step | -Using Ansys to get CL, Angle of Attack increasing by 5 degree  |


