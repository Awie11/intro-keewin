 Head | Information |
| ------ | ----------- |
| Logbook entry number | 03 |
| Name  | Kee Soon Win |
| Matrix no.  | 195901 |
| Year  | 4 |
| Subsystem  | Simulation |
| Group  | 7 |

| Target | Information |
| ------ | ----------- |
| Agenda | -Using CATIA to design HAU airframe<br> -Discuss and meeting with groupmates about the simulation's progress in terms of Real Time Measurements observing, data intake, Catia or Integrated Designing, Performance Calculations of HAU, CFD and Ansys.|
| Goals | -Design airframe with computer aided tool which is CATIA<br> -To carry out the design using the chosen ratio in order to achieve good Computational Fluid Dynamics results in the future.|
| Problems | -It is hard to know since didn't see the real airship instead of look it through virtually and listen to groupmates explain what happened in lab.<br> -CATIA was unable to comprehend specific times when the design process was taking place|
| Decision | -Design airship's airframe with simple measurements.<br> -using CATIA to design airframe|
| Method | -Design it in Catia with the measurements that have been set up<br> Note:To prevent a larger significance on the mistake involved, the measurement tolerance was chosen.|
| Justification | -Come out with good theory and concept for future design using catia<br> -Have good results and performance results using software.|
| Impact | -Come out with certain parameters such as Lift, Drag, Thrust and Weight.|
| Next Step | -Will proceed with airship performance by using CFD|










